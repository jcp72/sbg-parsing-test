DATA = [0x24, 0x00, 0x1a, 0x00, 0x3f, 0xd2, 0xcd, 0x1e, 0x06, 0x00, 0x00, 0xed, 0xc3, 0x47, 0x20, 0xee, 0xa8, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]

def calcCRC(bytes):
    POLY = 0x8408
    crc = 0
    carry = 0
    
    for j in range(0, len(bytes)):
        print(f"{j}: {crc}")
        crc = crc ^ bytes[j]
        print(f"{j} (byte {bytes[j]}): {crc}")

        for i in range(0, 8):
            carry = crc & 1
            # print(f"{j}/{i}: {crc}")
            crc = crc >> 1
            # print(f"{j}/{i}: {crc}")
            if (carry):
                crc = crc ^ POLY
            # print(f"{j}/{i}: {crc}")
    return crc

print(calcCRC(DATA))
