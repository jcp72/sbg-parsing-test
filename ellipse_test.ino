#include "sbg.hpp"
#define BURST_COUNT 5

#define READ_SERIAL Serial7 // for reading from the Ellipse
#define WRITE_SERIAL Serial // for writing to a computer/display

SbgStandardFrame* framePtr;
SystemData* systemDataPtr;

int frameCounter = 0;
int droppedFrameCounter = 0;
long byteCounter = 0;

// For serial printing
long lastMillis = 0;
long lastMillis2 = 0;
long bytesReceivedCounter = 0;
int burstCounter;

void setup() {
    READ_SERIAL.begin(115200);
    if (READ_SERIAL != WRITE_SERIAL) {
        WRITE_SERIAL.begin(115200);
        WRITE_SERIAL.println("Started Serial!");
    }
    // SbgStandardFrame frame;
    // framePtr = &frame;
    framePtr = (SbgStandardFrame*) malloc(sizeof(SbgStandardFrame));
    systemDataPtr = (SystemData*) malloc(sizeof(SystemData));
    // delay(1000);
    // WRITE_SERIAL.println("Frame data pointer:");
    // WRITE_SERIAL.println((int) &(frame.data));
    // WRITE_SERIAL.println("Attempting to write to frame data[20]");
    // frame.data[20] = 250;
    // WRITE_SERIAL.println(frame.data[20]);
    delay(1000);
    while (READ_SERIAL.available()) {
        // prevents spurious "BUFFER-FULL" type errors at the beginning of the script
        READ_SERIAL.read();
    }
    Serial.println("Ready...");
}

void printSystemStatus() {
    // Assuming we use about 10 bytes for printing the frame number,
    // we should stick to < 50 bytes for this part, so let's not print
    // everything every time.
    switch (burstCounter) {
        case 0:
            WRITE_SERIAL.print("aX aY aZ ");
            WRITE_SERIAL.print(systemDataPtr->IMUData.accelX_meters_per_s_2);
            WRITE_SERIAL.print(" ");
            WRITE_SERIAL.print(systemDataPtr->IMUData.accelY_meters_per_s_2);
            WRITE_SERIAL.print(" ");
            WRITE_SERIAL.println(systemDataPtr->IMUData.accelZ_meters_per_s_2);
            break;
        case 1:
            WRITE_SERIAL.print("gX gY gZ ");
            WRITE_SERIAL.print(systemDataPtr->IMUData.gyroX_radians_per_s);
            WRITE_SERIAL.print(" ");
            WRITE_SERIAL.print(systemDataPtr->IMUData.gyroY_radians_per_s);
            WRITE_SERIAL.print(" ");
            WRITE_SERIAL.println(systemDataPtr->IMUData.gyroZ_radians_per_s);
            break;
        case 2:
            WRITE_SERIAL.print("daX daY agZ ");
            WRITE_SERIAL.print(systemDataPtr->IMUData.deltaVelX_meters_per_s_2);
            WRITE_SERIAL.print(" ");
            WRITE_SERIAL.print(systemDataPtr->IMUData.deltaVelY_meters_per_s_2);
            WRITE_SERIAL.print(" ");
            WRITE_SERIAL.println(systemDataPtr->IMUData.deltaVelZ_meters_per_s_2);
            break;
        case 3:
            WRITE_SERIAL.print("dgX dgY dgZ ");
            WRITE_SERIAL.print(systemDataPtr->IMUData.deltaAngleX_radians_per_s);
            WRITE_SERIAL.print(" ");
            WRITE_SERIAL.print(systemDataPtr->IMUData.deltaAngleY_radians_per_s);
            WRITE_SERIAL.print(" ");
            WRITE_SERIAL.println(systemDataPtr->IMUData.deltaAngleZ_radians_per_s);
            break;
        case 4:
            WRITE_SERIAL.print("lat lon ");
            #if HAS_8_BYTE_FLOATS
                WRITE_SERIAL.print(systemDataPtr->EKFNav.latitude_degrees);
                WRITE_SERIAL.print(" ");
                WRITE_SERIAL.println(systemDataPtr->EKFNav.longitude_degrees);
            #else
                // need conversion to single precision (we lose a lot of position info here)
                WRITE_SERIAL.print(doubleToFloat(systemDataPtr->EKFNav.latitude_degrees));
                WRITE_SERIAL.print(" ");
                WRITE_SERIAL.println(doubleToFloat(systemDataPtr->EKFNav.longitude_degrees));
            #endif
            WRITE_SERIAL.print("sol pos typ ");
            // Solution status (Page 80)
            // Note: from binary log: 273 = 256 + 16 + 1 = (2^8 + 2^4 + 2^0) = vertical reference + valid attitude + vertical gyro mode (no heading)
            WRITE_SERIAL.print(systemDataPtr->EKFNav.solutionStatus);
            WRITE_SERIAL.print(" ");
            // GPS POS STATUS (Table 42, page 88)
            WRITE_SERIAL.print(systemDataPtr->GPS1Pos.gpsPosStatus & 0b00011111);
            WRITE_SERIAL.print(" ");
            // GPS POS TYPE   (Table 43, page 88)
            WRITE_SERIAL.print(systemDataPtr->GPS1Pos.gpsPosStatus & 0b0000111111000000);
            break;
        default:
            burstCounter = 0;
            break;
    }
}

void halt() {
    while (true) {
        Serial.println("Halting forever...");
        delay(500);
    }
}

void loop() {

    long now = millis();
    if (now - lastMillis > 500 && now - lastMillis2 > 25) {
        
        // update stats quickly twice a second
        
        if (now - lastMillis < 525) {
            WRITE_SERIAL.println();
            WRITE_SERIAL.print("\nF#");
            WRITE_SERIAL.print(frameCounter);
            WRITE_SERIAL.print(", B#");
            WRITE_SERIAL.println(bytesReceivedCounter);
        }

        lastMillis2 = now;
        printSystemStatus();

        burstCounter++;
        
        if (burstCounter == BURST_COUNT) {
            lastMillis = now;
            burstCounter = 0;
        }
        

    }

    while (READ_SERIAL.available()) {

        if (READ_SERIAL.available() > 60) {
            WRITE_SERIAL.println("READ_SERIAL is getting very full! This is BAD!!!");
        }
        
        byte myByte = READ_SERIAL.read();
        // WRITE_SERIAL.println(myByte);
        int result = processSbgByte(myByte, framePtr);
        bytesReceivedCounter += 1;

        if (result == SBG_PARSE_DONE) {
            frameCounter++;
            if (!frameCRCIsValid(framePtr)) {
                // Drop frame
                WRITE_SERIAL.println("Invalid CRC");
                droppedFrameCounter++;
            } else {
                frameCounter++;
                // byteCounter += (9 + framePtr->dataLength);
                if (framePtr->messageClass != 0) {
                    // Serial.print("Message with class ");
                    // Serial.print(framePtr->messageClass);
                    // Serial.println(" not decoded");
                } else {
                    // Serial.print("MESSAGE ID: ");
                    // Serial.println(framePtr->messageId);
                    // Binary output log (p.70 of interface specification)
                    retrieveBinaryLogInfo(framePtr, systemDataPtr);
                }
            }
        }
    }
}
