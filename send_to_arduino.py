import serial

arduino = serial.Serial(port='/dev/ttyACM0', baudrate=115200, timeout=1) 

# arduino.write(b"Hello worlds\n")

with open("20240117_01h14.log.bin", "rb") as file:
    file_bytes = file.read()

# arduino.write(file_bytes[68:170]) # one air data message + one IMU message
# arduino.write(file_bytes[68:103])
# arduino.write(file_bytes[68:200])
arduino.write(file_bytes[68:1000])
# import time

# for i in range(68, 103):
#     arduino.write([file_bytes[i]])
#     time.sleep(0.02)

# arduino.write(file_bytes)


print(f"Transferred byte(s).")
# print(f"Transferred {len(file_bytes)} byte(s).")

