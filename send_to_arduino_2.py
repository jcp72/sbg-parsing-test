#!/usr/bin/python3
import serial, time
from datetime import datetime
from typing import Dict

WRITE_TO_SERIAL = False # set to False to debug the Python script itself
REAL_TIME_EMULATION = True # set to False to run the code as fast as possible, ignoring message timestamps

# TODO: replace the string below with the port the arduino is connected to 
#       on your computer (on Windows, this is usually 'COM<#>', while 
#       on Linux and MacOS this is usually /dev/tty*)
PORT = "/dev/ttyACM0"

if WRITE_TO_SERIAL:
    arduino = serial.Serial(port=PORT, baudrate=115200, timeout=1) 

with open("20240117_01h14.log.bin", "rb") as file:
    file_bytes = file.read()

full_frames_only = file_bytes[0x44:0x1417c9]

byte_counter = 0

first_real_timestamp = datetime.now().timestamp()
last_real_timestamp = None
first_frame_timestamp = None
last_frame_timestamp = None

frame_counter = 0
binary_log_counter = 0

message_id_counter: Dict[int, int] = {}

print(" time elapsed | bytes sent |  byte/s  |  bits/s  | frames sent | avg.fr.size | frames/s ")

try:

    while byte_counter < len(full_frames_only):

        if full_frames_only[byte_counter] != 0xFF:
            raise ValueError()
        if full_frames_only[byte_counter + 1] != 0x5A:
            raise ValueError()

        frame_counter += 1

        loop_start_timestamp = datetime.now().timestamp()

        frame_message_id = full_frames_only[byte_counter + 2]
        frame_message_class = full_frames_only[byte_counter + 3]

        # Keep track of log types
        if frame_message_id not in message_id_counter:
            message_id_counter[frame_message_id] = 0
        message_id_counter[frame_message_id] += 1

        sleep_time_needed_seconds = 0

        if frame_message_class == 0:

            binary_log_counter += 1

            frame_timestamp = (
                full_frames_only[byte_counter + 6] |
                (full_frames_only[byte_counter + 7] << 8 ) |
                (full_frames_only[byte_counter + 8] << 16) |
                (full_frames_only[byte_counter + 9] << 24)
            )

            if first_frame_timestamp is not None and first_real_timestamp is not None:
                frame_delta_seconds = (frame_timestamp - first_frame_timestamp) / 1E6
                realtime_delta_seconds = loop_start_timestamp - first_real_timestamp
                if frame_delta_seconds > realtime_delta_seconds:
                    sleep_time_needed_seconds = (frame_delta_seconds - realtime_delta_seconds)

            if first_frame_timestamp is None:
                first_frame_timestamp = frame_timestamp

            last_frame_timestamp = frame_timestamp

        frame_data_length = full_frames_only[byte_counter + 4] | (full_frames_only[byte_counter + 5] << 8)
        
        frame_full_length = frame_data_length + 9

        if REAL_TIME_EMULATION:
            # Delay such that timings are similar to actual device
            time.sleep(sleep_time_needed_seconds)

        if WRITE_TO_SERIAL:
            ## SEND DATA HERE
            bytes_to_send = full_frames_only[byte_counter:byte_counter+frame_full_length]
            arduino.write(bytes_to_send)
            ##

        byte_counter += frame_full_length

        last_real_timestamp = datetime.now().timestamp()

        if first_real_timestamp is not None:
            time_elapsed_seconds = datetime.now().timestamp() - first_real_timestamp
            bytes_per_second = byte_counter / time_elapsed_seconds
            frames_per_second = frame_counter / time_elapsed_seconds
        else:
            time_elapsed_seconds = 0
            bytes_per_seconds = 0
            frames_per_second = 0

        bits_per_second = bytes_per_second * 8
        average_frame_size = byte_counter / frame_counter

        print(f"{time_elapsed_seconds:11.4f} s |{byte_counter:9d} B | {bytes_per_second:8.1f} | {bits_per_second:8.1f} | {frame_counter:11d} |{average_frame_size:12.2f} |{frames_per_second:8.2f}\r", end="")


except KeyboardInterrupt:
    pass

print()
print(f"{len(full_frames_only)} bytes transmitted ({frame_counter} frames, {binary_log_counter} of which were binary logs.)")

target_span = (last_frame_timestamp - first_frame_timestamp) / 1E6
actual_span = last_real_timestamp - first_real_timestamp

print(f"Target span was {target_span:.2f} seconds, actually took {actual_span:.2f} seconds.")

percent_bytes_sent = byte_counter / len(full_frames_only) * 100

print(f"{byte_counter} of {len(full_frames_only)} bytes from log sent ({percent_bytes_sent:6.2f}%)")
print("Count of each log received:", message_id_counter)